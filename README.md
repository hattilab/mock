# Mock
/mock
├── /src
│   ├── /controllers
│   │   ├── userController.js
│   │   └── productController.js
│   ├── /routes
│   │   ├── userRoutes.js
│   │   └── productRoutes.js
│   ├── /data
│   │   ├── userData.js
│   │   └── productData.js
│   └── /config
│       └── mockConfig.js
├── package.json
├── index.js
└── README.md
A pasta /src contém controladores, rotas e dados mockados específicos para a API de Mock. Agora, o código relacionado ao CORS e à instância do Express estará no Util.

Aqui estão alguns detalhes sobre cada diretório:

/src/controllers: Contém os controladores responsáveis por lidar com as requisições para cada rota da API de Mock. Por exemplo, o userController.js pode conter as funções para retornar dados de usuários mockados.

/src/routes: Contém as definições de rotas da API de Mock. Cada rota pode ser definida em um arquivo separado. Por exemplo, o userRoutes.js define as rotas relacionadas a usuários.

/src/data: Contém os arquivos que fornecem os dados mockados para os controladores. Por exemplo, o userData.js pode conter um array com dados de usuários fictícios.

/src/config: Contém o arquivo mockConfig.js, onde você pode importar a configuração do CORS do Util e configurar a instância do Express utilizando o middleware do CORS.

index.js: É o ponto de entrada da API de Mock. Nele, você importará a instância do Express configurada com o CORS e definirá o uso das rotas e as configurações adicionais, como a porta na qual a API será executada.

README.md: Contém a documentação da API de Mock, descrevendo as rotas disponíveis, os endpoints, os dados mockados e outras informações relevantes para o desenvolvimento e os testes.

Ao utilizar essa estrutura, você poderá reutilizar a configuração do CORS e a instância do Express do Util para a API de Mock. Isso ajudará a manter a consistência e a reduzir a duplicação de código.

Certifique-se de atualizar a documentação da API de Mock para informar os desenvolvedores sobre como utilizar o Util para a configuração do CORS e do Express.

Lembre-se de seguir as melhores práticas de organização de código e modularização, garantindo que cada parte da API de Mock esteja devidamente separada e bem definida.


- [ ] Definir as dependências 
  - [ ] Bibliotecas
    - [X] Jest
    - [ ] Outras bibliotecas necessárias
  - [ ] Ferramentas
    - [ ] Outras ferramentas necessárias
- [ ] Configurar o projeto
  - [X] Criar uma pasta para a API de Mock
  - [X] Iniciar um novo projeto Node.js executando o comando `npm init` no terminal
- [ ] Instalar as dependências
  - [X] Instalar as dependências necessárias para a implementação da API de Mock, por exemplo, executar o comando `npm install jest` no terminal
- [ ] Criar o arquivo de entrada
  - [X] Criar um arquivo `index.js` como ponto de entrada da API de Mock
    - [X] Configurar o servidor Express
    - [X] Iniciar o servidor
- [X] Configurar as rotas
  - [X] Rota `/users`
    - [X] `GET /users`: Retorna uma lista de usuários fictícios
    - [X] `GET /users/:id`: Retorna os detalhes de um usuário específico com base no ID
    - [X] `POST /users`: Cria um novo usuário com base nos dados fornecidos no corpo da requisição
    - [X] `PUT /users/:id`: Atualiza os dados de um usuário específico com base no ID
    - [X] `DELETE /users/:id`: Remove um usuário específico com base no ID
  - [X] Rota `/products`
    - [X] `GET /products`: Retorna uma lista de produtos fictícios
    - [X] `GET /products/:id`: Retorna os detalhes de um produto específico com base no ID
    - [X] `POST /products`: Cria um produto com base nos dados fornecidos no corpo da requisição
    - [X] `PUT /products/:id`: Atualiza os dados de um produto específico com base no ID
    - [X] `DELETE /products/:id`: Remove um produto específico com base no ID
- [X] Implementar os controladores
  - [X] Criar os controladores correspondentes para cada rota
- [X] Configurar as rotas no diretório `/src/routes`
    - [X] Criar os arquivos de rotas correspondentes a cada recurso ou grupo de rotas
      - [X] `userRoutes.js` para as definições das rotas relacionadas a usuários
      - [X] `productRoutes.js` para as definições das rotas relacionadas a produtos
    - [X] Utilizar os controladores correspondentes para cada rota
- [X] Configurar a instância do Express no arquivo `index.js`
  - [X] Importar a instância do Express do Util
  - [X] Configurar as rotas da API de Mock utilizando as rotas definidas anteriormente
  - [X] Definir o uso do middleware do CORS utilizando a configuração do CORS do Util
- [X] Iniciar o servidor Express
  - [X] Configurar o servidor para ouvir numa porta específica no arquivo `index.js`
- [ ] Testar a API de Mock
  - [ ] Executar a API de Mock e testar as rotas e endpoints utilizando uma ferramenta como o Postman ou o cURL
- [X] Documentar a API de Mock
  - [X] Criar a documentação descrevendo as rotas disponíveis, os endpoints, os parâmetros esperados e as respostas mockadas
- [ ] Refinar e expandir a API de Mock
  - [ ] Adicionar novas rotas, endpoints e funcionalidades conforme necessário
  - [ ] Testar e validar as novas implementações


Certamente! Aqui está um exemplo básico de como a documentação da API de Mock pode ser estruturada em formato Markdown:

# Documentação da API de Mock

A API de Mock é responsável por simular dados e respostas para fins de desenvolvimento e testes. Através dessa API, é possível simular diferentes rotas e endpoints para facilitar o desenvolvimento de outras partes do projeto.

## Visão Geral

A API de Mock permite simular dados e respostas para as seguintes entidades:

- Usuários
- Produtos

## Endpoints Disponíveis

### Usuários

- `GET /users`: Retorna todos os usuários cadastrados.
- `GET /users/{id}`: Retorna um usuário específico pelo ID.
- `POST /users`: Cria um novo usuário.
- `PUT /users/{id}`: Atualiza um usuário existente pelo ID.
- `DELETE /users/{id}`: Exclui um usuário pelo ID.

### Produtos

- `GET /products`: Retorna todos os produtos cadastrados.
- `GET /products/{id}`: Retorna um produto específico pelo ID.
- `POST /products`: Cria um novo produto.
- `PUT /products/{id}`: Atualiza um produto existente pelo ID.
- `DELETE /products/{id}`: Exclui um produto pelo ID.

## Parâmetros de Requisição

Os seguintes parâmetros podem ser utilizados nas requisições para a API de Mock:

- `id` (path): ID do usuário ou produto desejado.
- `name` (query ou body): Nome do usuário ou produto.
- `email` (query ou body): E-mail do usuário.
- `price` (query ou body): Preço do produto.

## Exemplos de Uso

Abaixo estão exemplos de uso da API de Mock:

### Obter todos os usuários

**Requisição:**
```
GET /users
```

**Resposta:**
```
Status: 200 OK

[
  {
    "id": 1,
    "name": "John Doe",
    "email": "john.doe@example.com"
  },
  {
    "id": 2,
    "name": "Jane Smith",
    "email": "jane.smith@example.com"
  }
]
```

### Criar um novo produto

**Requisição:**
```
POST /products

{
  "name": "Product 1",
  "price": 19.99
}
```

**Resposta:**
```
Status: 201 Created
```

## Considerações de Segurança

A API de Mock não requer autenticação ou autorização para acessar os endpoints. Porém, é importante lembrar que essa API é voltada para desenvolvimento e testes, e não deve ser utilizada em ambiente de produção.

## Outras Informações

Para mais informações sobre a API de Mock e detalhes adicionais sobre os endpoints e parâmetros, consulte a documentação completa disponível no repositório do projeto.



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/hattilab/mock.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/hattilab/mock/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
