const app = require('../util');

const port = 3002;

// Definir o uso das rotas da API de Mock
app.use('/clients', app.routes.clientRoutes);
app.use('/users', app.routes.userRoutes);
app.use('/products', app.routes.productRoutes);

// Iniciar o servidor
app.listen(port, () => {
  console.log(`API de Mock rodando em http://localhost:${port}`);
});
